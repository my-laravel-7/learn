# CÀI ĐẶT VÀ CẤU TRÚC THƯ MỤC

## Mục lục

>- [1.Cài đặt Laravel](https://gitlab.com/my-laravel-7/learn/-/blob/master/huong_dan.md#1c%C3%A0i-%C4%91%E1%BA%B7t-laravel-top)
>- [2.Chạy Local Source Laravel](https://gitlab.com/my-laravel-7/learn/-/blob/master/huong_dan.md#2ch%E1%BA%A1y-local-source-laravel-top)
>- [3.Cấu trúc thư mục](https://gitlab.com/my-laravel-7/learn/-/blob/master/huong_dan.md#3c%E1%BA%A5u-tr%C3%BAc-th%C6%B0-m%E1%BB%A5c-top)
>- [4.Các lệnh trong Laravel](https://gitlab.com/my-laravel-7/learn/-/blob/master/huong_dan.md#4c%C3%A1c-l%E1%BB%87nh-trong-laravel-top)
>	- [4.1.Danh sách lệnh](https://gitlab.com/my-laravel-7/learn/-/blob/master/huong_dan.md#41danh-s%C3%A1ch-l%E1%BB%87nh-command-top)
>	- [4.2.Facade Class Reference](https://gitlab.com/my-laravel-7/learn/-/blob/master/huong_dan.md#42facade-class-reference-top)
>	- [4.3.Contract Reference](https://gitlab.com/my-laravel-7/learn/-/blob/master/huong_dan.md#43contract-reference-top)

## Info

>- Link Local

	C:\OS_Eglobal\OSPanel\domains\laravel7\learn
		
>- Link GitLab

	https://gitlab.com/my-laravel-7/learn.git
	
>- Lệnh Git commit

	git add . && git commit -m "lesson" && git po


## 1.Cài đặt Laravel ([TOP^](#))
 - Đến thư mục gốc Local Host, chạy lệnh sau:
	```php
	composer create-project --prefer-dist laravel/laravel:^7.0 project_name
	```
	+ **`project_name`** là tên thư mục source muốn cài
	+ **`^7.0`** là version laravel
	
## 2.Chạy Local Source Laravel ([TOP^](#))
 - Để chạy Source Local ta chạy lệnh sau:
	```php
	php artisan serve
	```
- Khi đó mở trình duyệt web và truy cập vào link
	http://localhost:8000 hoặc http://127.0.0.1:8000

## 3.Cấu trúc thư mục  ([TOP^](#))
>- app
>	- Console
>		- Kernel.php
>	- Exceptions
>		- Handler.php
>	- Http
>		- Controllers
>		- Middleware
>		- Kernel.php
>	- Providers
>- bootstrap
>	- cache
>	- app.php
>- config
>- database
>	- factories
>	- migrations
>	- seeds
>- public
>- resources
>	- js
> 	- lang
> 	- sass
> 	- views
>- routes
>	- web.php
>	- api.php
>	- console.php
>	- channels.php
>- storage
>	- app
>		- public
>	- framework
> 	- logs
>- tests
>- vendor

- **`app`** chứa mã Core của web. **`Console`** và **`Http`** cung cấp API cho ứng dụng của bạn.Giao thức HTTP và CLI đều là cơ chế tương tác với ứng dụng của bạn, nhưng không thực sự chứa logic ứng dụng.Nói cách khác, chúng là hai cách phát lệnh cho ứng dụng của bạn.Thư mục **`Console`** chứa tất cả các lệnh Artisan của bạn, trong khi thư mục **`Http`** chứa các controller, middleware và các request của bạn. Một loạt các thư mục mặc định sẽ không được tạo ra. Nó sẽ được tạp ra bằng lệnh **`make`** của Artisan . Để biết danh sách lệnh ta dùng 
	```php
	php artisan list make
	```	
	- **`Console`** :Thư mục **`Console`** chứa tất cả các lệnh Artisan tùy chỉnh cho ứng dụng của bạn. Các lệnh này có thể được tạo bằng lệnh make:command này cũng chứa nhân bảng điều khiển của bạn, đây là nơi đăng ký các lệnh Artisan tùy chỉnh của bạn và các tác vụ đã lên lịch của bạn được xác định
		```php
		php artisan make:command ClassName
		```
		Tham khảo tại: https://laravel.com/docs/7.x/scheduling
	- **`Exceptions`** : Thư mục **`Exceptions`** chứa trình xử lý ngoại lệ của ứng dụng của bạn và cũng là một nơi tốt để đặt bất kỳ ngoại lệ nào do ứng dụng của bạn đưa ra. Nếu bạn muốn tùy chỉnh cách các ngoại lệ của bạn được ghi lại hoặc hiển thị, bạn nên sửa đổi lớp **`Handler`** trong thư mục này . Hoặc có thể tạo mới bằng lệnh sau:
		```php
		php artisan make:exception ClassName
		```
	- **`Http`** : Thư mục **`Http`** chứa các bộ controller, middleware và các request biểu mẫu của bạn. Hầu như tất cả logic để xử lý các yêu cầu nhập vào ứng dụng của bạn sẽ được đặt trong thư mục này.
	- **`Providers`**
	- **`Broadcasting`** : là thư mục chứa ứng dụng liên quan đến websocket thời gian thực . Thực hiện tạo bằng lệnh
		```php
		php artisan make:channel ClassName
		```
		Tham khảo tại: https://laravel.com/docs/7.x/broadcasting
	- **`Events`** : Thư mục này không tồn tại theo mặc định, nhưng sẽ được tạo cho bạn bằng lệnh **`event:generate`** và **`make:event`** Artisan. Thư mục **`Events`** chứa các lớp sự kiện. Các sự kiện có thể được sử dụng để cảnh báo các phần khác trong ứng dụng của bạn rằng một hành động nhất định đã xảy ra, mang lại sự linh hoạt và khả năng tách
		```php
		php artisan make:event ClassName
		```
		Tham khảo tại: https://laravel.com/docs/7.x/events
	- **`Jobs`** : Thư mục này không tồn tại theo mặc định, nhưng sẽ được tạo cho bạn nếu bạn thực hiện lệnh **`make:job`** Artisan. Thư mục **`Jobs`**  chứa các công việc có thể xếp [hàng đợi](https://laravel.com/docs/7.x/queues) cho ứng dụng của bạn. Công việc có thể được ứng dụng của bạn xếp hàng đợi hoặc chạy đồng bộ trong vòng đời yêu cầu hiện tại. Các công việc chạy đồng bộ trong quá trình yêu cầu hiện tại đôi khi được gọi là "lệnh" vì chúng là sự triển khai của [mẫu lệnh](https://en.wikipedia.org/wiki/Command_pattern)
		```php
		php artisan make:job ClassName
		```
	- **`Listeners`** : Thư mục này không tồn tại theo mặc định, nhưng sẽ được tạo cho bạn bằng lệnh **`event:generate`** và **`make:listener`** Artisan. Thư mục **`Listeners`** chứa các lớp xử lý các [sự kiện](https://laravel.com/docs/7.x/events) của bạn. Người nghe sự kiện nhận được một cá thể sự kiện và thực hiện logic để phản hồi lại sự kiện đang được kích hoạt. Ví dụ: sự kiện **`UserRegistered`** có thể được xử lý bởi trình nghe **`SendWelcomeEmail`**
		```php
		php artisan make:listener ClassName
		```
	- **`Mail`** : Thư mục này không tồn tại theo mặc định, nhưng sẽ được tạo cho bạn bằng lệnh **`make:mail`** Artisan.Thư mục **`Mail`** chứa tất cả các lớp của bạn đại diện cho các email được gửi bởi ứng dụng của bạn. Đối tượng Mail cho phép bạn đóng gói tất cả logic của việc xây dựng email trong một lớp đơn giản, có thể được gửi bằng phương thức **`Mail::send`**. 
		```php
		php artisan make:mail ClassName
		```
	- **`Notifications`** : Thư mục này không tồn tại theo mặc định, nhưng sẽ được tạo cho bạn bằng lệnh **`make:notification`** Artisan. Thư mục **`Notifications`** chứa tất cả các thông báo "giao dịch" được gửi bởi ứng dụng của bạn, chẳng hạn như thông báo đơn giản về các sự kiện xảy ra trong ứng dụng của bạn. Tính năng thông báo của Laravel tóm tắt việc gửi thông báo qua nhiều trình điều khiển như email, Slack, SMS hoặc được lưu trữ trong cơ sở dữ liệu.
		```php
		php artisan make:notification ClassName
		```
	- **`Providers`** : Thư mục **`Providers`** chứa tất cả [các nhà cung cấp dịch vụ](https://laravel.com/docs/7.x/providers) cho ứng dụng của bạn. Các nhà cung cấp dịch vụ khởi động ứng dụng của bạn bằng cách ràng buộc các dịch vụ trong vùng chứa dịch vụ, đăng ký sự kiện hoặc thực hiện bất kỳ tác vụ nào khác để chuẩn bị ứng dụng của bạn cho các yêu cầu đến.
	Trong một ứng dụng Laravel mới, thư mục này sẽ chứa một số trình cung cấp. Bạn có thể tự do thêm các nhà cung cấp của riêng mình vào thư mục này nếu cần
	- **`Rules`** : Thư mục này không tồn tại theo mặc định, nhưng sẽ được tạo cho bạn bằng lệnh **`make:rule`** Artisan.Thư mục **`Rules`** chứa các đối tượng quy tắc xác thực tùy chỉnh cho ứng dụng của bạn. Các quy tắc được sử dụng để đóng gói logic xác nhận phức tạp trong một đối tượng đơn giản. Để biết thêm thông tin, hãy xem [tài liệu xác nhận](https://laravel.com/docs/7.x/validation) 	
		```php
		php artisan make:rule ClassName
		```
- **`bootstrap`** : chứa các khởi động và cache
	- **`app.php`** để khởi động laravel
	- **`cache`** là thư mục chứa cache của route hay file cache
- **`config`** là thư mục chứa các cấu hình web trong các file.
- **`database`** liên quan đến tạo ,sửa CSDL ,model, dữ liệu mẫu
	- **`migrations`** , kiểu như định nghĩa các bảng CSDL
- **`public`** chứa file **`index.php`** đây sẽ là file tiếp nhận các yêu cầu của người dùng và trả về hiển thị. Thư mục này chứa các thư mục liên quan đến hình ảnh, CSS, Javascript
- **`resources`** chứa các code thô của bạn , chưa qua biên dịch hoặc chưa nén lại như LESS, SASS, JS, file ngôn ngữ, View
- **`routes`** chứa các file định tuyến cho ứng dụng.
	- **`web.php`** để định nghĩa định tuyến cho website
	- **`api.php`** để định nghĩa định tuyến cho API
	- **`console.php`** để định nghĩa định tuyến cho các lệnh ở cửa sổ lệnh
	- **`channels.php`** để định nghĩa định tuyến cho các kênh 
- **`storage`** : chứa mẫu Blade đã biên dịch, session, cache, log
	- **`app`**: lưu trữ các file đã được tạo ra trong ứng dụng 
		- **`public`**: chứa các tệp của người dùng tạo như hình ảnh, css, js. chúng ta có thể tạo liên kết tại **`public/storage`** đến thư mục này. Sử dụng lệnh 
		```php
		php artisan storage:link
		```
	- **`framework `**: lưu trữ các file , bộ nhớ đệm
	- **`logs `**: lưu trữ các log
- **`tests`** chứa kiểm tra tự động. https://phpunit.de/ là link về kiểm tra đơn vị trong PHP. Mỗi class kiểm tra phải gắn với hậu tố **`Test`**. Bạn có thể chạy kiểm thử bằng lệnh **`phpunit`** hoặc **`php vendor/bin/phpunit`**
- **`vendor`** chứa các file cài đặt bằng lệnh **`composer`**
## 4.Các lệnh trong Laravel ([TOP^](#))

- Để chạy lệnh thì ta dùng 

	```php
	php artisan <command> ClassName
	```

	+ **`<command>`** như danh sách bên dưới
### 4.1.Danh sách lệnh **`<command>`**  ([TOP^](#))

|Command|description|Path|
|---|---|---|
|**make:cast**|Create a new custom Eloquent cast class|**`app/Casts`**|
|**make:channel**|Create a new channel class|**`app/Broadcasting`**|
|**make:command**|Create a new Artisan command|**`app/Console/Commands`**|
|**make:component**|Create a new view component class|**`app/View/Components , resources/views/components`**|
|**make:controller**|Create a new controller class|**`app/Http/Controllers`**|
|**make:event**|Create a new event class|**`app/Events`**|
|**make:exception**|Create a new custom exception class|**`app/Exceptions`**|
|**make:factory**|Create a new model factory|**`database/factories`**|
|**make:job**|Create a new job class|**`app/Jobs`**|
|**make:listener**|Create a new event listener class|**`app/Listeners`**|
|**make:mail**|Create a new email class|**`app/Mail`**|
|**make:middleware**|Create a new middleware class|**`app/Http/Middleware`**|
|**make:migration**|Create a new migration file|**`database/migrations`**|
|**make:model**|Create a new Eloquent model class|**`app/`**|
|**make:notification**|Create a new notification class|**`app/Notifications`**|
|**make:observer**|Create a new observer class|**`app/Observers`**|
|**make:policy**|Create a new policy class|**`app/Policies`**|
|**make:provider**|Create a new service provider class|**`app/Providers`**|
|**make:request**|Create a new form request class|**`app/Http/Requests`**|
|**make:resource**|Create a new resource|**`app/Http/Resources`**|
|**make:rule**|Create a new validation rule|**`app/Rules`**|
|**make:seeder**|Create a new seeder class|**`database/seeds`**|
|**make:test**|Create a new test class|**`tests/Feature`**|

### 4.2.Facade Class Reference ([TOP^](#))

|Facade|Class|Service Container Binding|
|---|---|---|
|App|Illuminate\Foundation\Application|**`app`**|
|Artisan|Illuminate\Contracts\Console\Kernel|**`artisan`**|
|Auth|Illuminate\Auth\AuthManager|**`auth`**|
|Auth (Instance)|Illuminate\Contracts\Auth\Guard|**`auth.driver`**|
|Blade|Illuminate\View\Compilers\BladeCompiler|**`blade.compiler`**|
|Broadcast|Illuminate\Contracts\Broadcasting\Factory|**``**|
|Broadcast (Instance)|Illuminate\Contracts\Broadcasting\Broadcaster|**``**|
|Bus|Illuminate\Contracts\Bus\Dispatcher|**``**|
|Cache|Illuminate\Cache\CacheManager|**`cache`**|
|Cache (Instance)|Illuminate\Cache\Repository|**`cache.store`**|
|Config|Illuminate\Config\Repository|**`config`**|
|Cookie|Illuminate\Cookie\CookieJar|**`cookie`**|
|Crypt|Illuminate\Encryption\Encrypter|**`encrypter`**|
|DB|Illuminate\Database\DatabaseManager|**`db`**|
|DB (Instance)|Illuminate\Database\Connection|**`db.connection`**|
|Event|Illuminate\Events\Dispatcher|**`events`**|
|File|Illuminate\Filesystem\Filesystem|**`files`**|
|Gate|Illuminate\Contracts\Auth\Access\Gate|**``**|
|Hash|Illuminate\Contracts\Hashing\Hasher|**`hash`**|
|Http|Illuminate\Http\Client\Factory|**``**|
|Lang|Illuminate\Translation\Translator|**`translator`**|
|Log|Illuminate\Log\LogManager|**`log`**|
|Mail|Illuminate\Mail\Mailer|**`mailer`**|
|Notification|Illuminate\Notifications\ChannelManager|**``**|
|Password|Illuminate\Auth\Passwords\PasswordBrokerManager|**`auth.password`**|
|Password (Instance)|Illuminate\Auth\Passwords\PasswordBroker|**`auth.password.broker`**|
|Queue|Illuminate\Queue\QueueManager|**`queue`**|
|Queue (Instance)|Illuminate\Contracts\Queue\Queue|**`queue.connection`**|
|Queue (Base Class)|Illuminate\Queue\Queue|**``**|
|Redirect|Illuminate\Routing\Redirector|**`redirect`**|
|Redis|Illuminate\Redis\RedisManager|**`redis`**|
|Redis (Instance)|Illuminate\Redis\Connections\Connection|**`redis.connection`**|
|Request|Illuminate\Http\Request|**`request`**|
|Response|Illuminate\Contracts\Routing\ResponseFactory|**``**|
|Response (Instance)|Illuminate\Http\Response|**``**|
|Route|Illuminate\Routing\Router|**`router`**|
|Schema|Illuminate\Database\Schema\Builder|**``**|
|Session|Illuminate\Session\SessionManager|**`session`**|
|Session (Instance)|Illuminate\Session\Store|**`session.store`**|
|Storage|Illuminate\Filesystem\FilesystemManager|**`filesystem`**|
|Storage (Instance)|Illuminate\Contracts\Filesystem\Filesystem|**`filesystem.disk`**|
|URL|Illuminate\Routing\UrlGenerator|**`url`**|
|Validator|Illuminate\Validation\Factory|**`validator`**|
|Validator (Instance)|Illuminate\Validation\Validator|**``**|
|View|Illuminate\View\Factory|**`view`**|
|View (Instance)|Illuminate\View\View|**``**|

### 4.3.Contract Reference ([TOP^](#))

|Contract|References Facade|
|---|---|
|Illuminate\Contracts\Auth\Access\Authorizable|**``**|
|Illuminate\Contracts\Auth\Access\Gate|**`Gate`**|
|Illuminate\Contracts\Auth\Authenticatable|**``**|
|Illuminate\Contracts\Auth\CanResetPassword|**``**|
|Illuminate\Contracts\Auth\Factory|**`Auth`**|
|Illuminate\Contracts\Auth\Guard|**`Auth::guard()`**|
|Illuminate\Contracts\Auth\PasswordBroker|**`Password::broker()`**|
|Illuminate\Contracts\Auth\PasswordBrokerFactory|**`Password`**|
|Illuminate\Contracts\Auth\StatefulGuard|**``**|
|Illuminate\Contracts\Auth\SupportsBasicAuth|**``**|
|Illuminate\Contracts\Auth\UserProvider|**``**|
|Illuminate\Contracts\Bus\Dispatcher|**`Bus`**|
|Illuminate\Contracts\Bus\QueueingDispatcher|**`Bus::dispatchToQueue()`**|
|Illuminate\Contracts\Broadcasting\Factory|**`Broadcast`**|
|Illuminate\Contracts\Broadcasting\Broadcaster|**`Broadcast::connection()`**|
|Illuminate\Contracts\Broadcasting\ShouldBroadcast|**``**|
|Illuminate\Contracts\Broadcasting\ShouldBroadcastNow|**``**|
|Illuminate\Contracts\Cache\Factory|**`Cache`**|
|Illuminate\Contracts\Cache\Lock|**``**|
|Illuminate\Contracts\Cache\LockProvider|**``**|
|Illuminate\Contracts\Cache\Repository|**`Cache::driver()`**|
|Illuminate\Contracts\Cache\Store|**``**|
|Illuminate\Contracts\Config\Repository|**`Config`**|
|Illuminate\Contracts\Console\Application|**``**|
|Illuminate\Contracts\Console\Kernel|**`Artisan`**|
|Illuminate\Contracts\Container\Container|**`App`**|
|Illuminate\Contracts\Cookie\Factory|**`Cookie`**|
|Illuminate\Contracts\Cookie\QueueingFactory|**`Cookie::queue()`**|
|Illuminate\Contracts\Database\ModelIdentifier|**``**|
|Illuminate\Contracts\Debug\ExceptionHandler|**``**|
|Illuminate\Contracts\Encryption\Encrypter|**`Crypt`**|
|Illuminate\Contracts\Events\Dispatcher|**`Event`**|
|Illuminate\Contracts\Filesystem\Cloud|**`Storage::cloud()`**|
|Illuminate\Contracts\Filesystem\Factory|**`Storage`**|
|Illuminate\Contracts\Filesystem\Filesystem|**`Storage::disk()`**|
|Illuminate\Contracts\Foundation\Application|**`App`**|
|Illuminate\Contracts\Hashing\Hasher|**`Hash`**|
|Illuminate\Contracts\Http\Kernel|**``**|
|Illuminate\Contracts\Mail\MailQueue|**`Mail::queue()`**|
|Illuminate\Contracts\Mail\Mailable|**``**|
|Illuminate\Contracts\Mail\Mailer|**`Mail`**|
|Illuminate\Contracts\Notifications\Dispatcher|**`Notification`**|
|Illuminate\Contracts\Notifications\Factory|**`Notification`**|
|Illuminate\Contracts\Pagination\LengthAwarePaginator|**``**|
|Illuminate\Contracts\Pagination\Paginator|**``**|
|Illuminate\Contracts\Pipeline\Hub|**``**|
|Illuminate\Contracts\Pipeline\Pipeline|**``**|
|Illuminate\Contracts\Queue\EntityResolver|**``**|
|Illuminate\Contracts\Queue\Factory|**`Queue`**|
|Illuminate\Contracts\Queue\Job|**``**|
|Illuminate\Contracts\Queue\Monitor|**`Queue`**|
|Illuminate\Contracts\Queue\Queue|**`Queue::connection()`**|
|Illuminate\Contracts\Queue\QueueableCollection|**``**|
|Illuminate\Contracts\Queue\QueueableEntity|**``**|
|Illuminate\Contracts\Queue\ShouldQueue|**``**|
|Illuminate\Contracts\Redis\Factory|**`Redis`**|
|Illuminate\Contracts\Routing\BindingRegistrar|**`Route`**|
|Illuminate\Contracts\Routing\Registrar|**`Route`**|
|Illuminate\Contracts\Routing\ResponseFactory|**`Response`**|
|Illuminate\Contracts\Routing\UrlGenerator|**`URL`**|
|Illuminate\Contracts\Routing\UrlRoutable|**``**|
|Illuminate\Contracts\Session\Session|**`Session::driver()`**|
|Illuminate\Contracts\Support\Arrayable|**``**|
|Illuminate\Contracts\Support\Htmlable|**``**|
|Illuminate\Contracts\Support\Jsonable|**``**|
|Illuminate\Contracts\Support\MessageBag|**``**|
|Illuminate\Contracts\Support\MessageProvider|**``**|
|Illuminate\Contracts\Support\Renderable|**``**|
|Illuminate\Contracts\Support\Responsable|**``**|
|Illuminate\Contracts\Translation\Loader|**``**|
|Illuminate\Contracts\Translation\Translator|**`Lang`**|
|Illuminate\Contracts\Validation\Factory|**`Validator`**|
|Illuminate\Contracts\Validation\ImplicitRule|**``**|
|Illuminate\Contracts\Validation\Rule|**``**|
|Illuminate\Contracts\Validation\ValidatesWhenResolved|**``**|
|Illuminate\Contracts\Validation\Validator|**`Validator::make()`**|
|Illuminate\Contracts\View\Engine|**``**|
|Illuminate\Contracts\View\Factory|**`View`**|
|Illuminate\Contracts\View\View|**`View::make()`**|

|[Danh sách bài học](https://gitlab.com/my-laravel-7/learn/-/blob/master/__list_lesson.md)|[Bài tiếp](https://gitlab.com/my-laravel-7/learn/-/blob/lesson1/huong_dan.md)|
|---|---|