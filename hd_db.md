# Database

## Mục Lục

> - [1. Khái niệm](https://gitlab.com/my-laravel-7/learn/-/blob/master/hd_db.md#1-kh%C3%A1i-ni%E1%BB%87m-top)
> - [2.Cách dùng](https://gitlab.com/my-laravel-7/learn/-/blob/master/hd_db.md#2c%C3%A1ch-d%C3%B9ng-top)
> - [3. Ưu nhược điểm](https://gitlab.com/my-laravel-7/learn/-/blob/master/hd_db.md#3-%C6%B0u-nh%C6%B0%E1%BB%A3c-%C4%91i%E1%BB%83m-top)
> - [4. Các lệnh](https://gitlab.com/my-laravel-7/learn/-/blob/master/hd_db.md#4-c%C3%A1c-l%E1%BB%87nh-top)

## 1. Khái niệm [TOP^](#)

| Eloquent | QueryBuilder |
|---|---|
|<details><summary>Eloquent là gì?</summary>Eloquent được Laravel cung cấp để lập trình viên tương tác với database 1 cách rễ ràng và đơn giản.<br> Cụ thể, Eloquent đi kèm theo Laravel được cung cấp ActiveRecord đầy đủ. Mỗi 1 bảng ở database sẽ được ánh sạ với 1 model, và model này được sử dụng để tương tác với bảng</details>|<details><summary>QueryBuilder là gì?</summary>QueryBuilder được Laravel cung cấp 1 giao diện thuận tiện giúp lập trình viên dễ dàng chạy các truy vấn tới database. <br>Nó có thể được sử dụng để thực thi hầu hết các thao tác về database</details>|

## 2.Cách dùng [TOP^](#)

|Các bước| Eloquent | QueryBuilder |
|---|---|---|
|1. Tạo Model|<details><summary>*(xem chi tiết:)*</summary>- Tạo model bằng lệnh **`php artisan make:model ModelName`** (**`ModelName`** thường là số ít)<br>- Model với tên **`ModelName`** vừa tạo bắt buộc có những lệnh sau: 	<br>  + **`use Illuminate\Database\Eloquent\Model;`** <br>  + **class `ModelName` extends Model** chính là tên model vừa tạo (**`ModelName`** thường là số ít)<br>  + **protected $table = '`ModelNames`';** `ModelNames` là số nhiều, chính là tên bảng trong Database</details>|Không cần bước này|
|2. Khai báo ở **`Controller`**|<details><summary>*(xem chi tiết:)*</summary>- Khai báo dòng lệnh sau ở đầu file Controller **use App\\`ModelName`;** (**`ModelName`** thường là số ít, là Model vừa tạo của bảng đó)</details>|<details><summary>*(xem chi tiết:)*</summary>- sử dụng lệnh này ở đầu **`Controller`**:<br>  + **use `DB`;**</details>|
|3. Sử dụng lệnh trong **`Controller`**|<details><summary>*(xem chi tiết:)*</summary>- **`ModelName::`Command_1()->..<br>->Command_N();** <br>(Tùy từng lệnh sẽ có các lệnh khác nhau ở **`Command_N`**)</details>|<details><summary>*(xem chi tiết:)*</summary>- **`DB::table`('`TableName`')->Command_1()->..<br>->Command_N();** <br>(**TableName** là tên Table trong database. Tùy từng lệnh sẽ có các lệnh khác nhau ở **`Command_N`**)</details>|

## 3. Ưu nhược điểm [TOP^](#)

|| Eloquent | QueryBuilder |
|---|---|---|
|**`Ưu điểm`**|- Khả năng tái sử dụng hàm cao.<br> - Eloquent dễ dàng liên kết giữa các bảng trong database hơn Query builder. <br>- sử dụng : 'PDO parameter binding' nên sẽ giúp chúng ta tránh được lỗi sql injection.|- Tốc độ truy vấn nhanh hơn Eloquent<br>- Không cần tạo model vẫn có thể truy vấn.<br>- sử dụng : 'PDO parameter binding' nên sẽ giúp chúng ta tránh được lỗi sql injection.|
|**`Nhược điểm`**|- Cần tạo model trước khi truy vấn. <br>- Nếu ở Controller cần sử dụng thì dùng lệnh **`use App\ModelName`** đó vào nếu không sẽ xảy ra lỗi, tốc độ thực thi chậm hơn QueryBuider.|- Không tái sử dụng được các hàm|

## 4. Các lệnh [TOP^](#)

|Lệnh| Eloquent | QueryBuilder |
|---|---|---|
|**`Lấy Tất cả Record của 1 bảng`**|<details><summary>*(xem chi tiết:)*</summary>**`ModelName::`all();**</details>|<details><summary>*(xem chi tiết:)*</summary>**`DB::table('TableName')`->get();**</details>|
|**`Lấy 1 Record theo id`**|<details><summary>*(xem chi tiết:)*</summary>**`ModelName::`find(id_value);** <br> **`id_value`** là giá trị số của field ID của ModelName</details>|<details><summary>*(xem chi tiết:)*</summary>**`DB::table('TableName')`->where('id', id_value)->first();**<br> **`id_value`** là giá trị số của field ID của bảng TableName</details>|
|**`Lấy 1 Field của 1 Record theo ID`**|<details><summary>*(xem chi tiết:)*</summary>**`ModelName::`where('id', id_value)->`value('FieldName')`;** <br> **`FieldName`** là tên trường muốn lấy dữ liệu của ModelName</details>|<details><summary>*(xem chi tiết:)*</summary>**`DB::table('TableName')`->where('id', id_value)->`value('FieldName')`;**<br> **`FieldName`** là tên trường muốn lấy dữ liệu của bảng TableName</details>|
|**`Lấy 1 Field của tất cả Record`**|<details><summary>*(xem chi tiết:)*</summary>**`ModelName::` `lists('FieldName')`;** <br> **`FieldName`** là tên trường muốn lấy dữ liệu của ModelName</details>|<details><summary>*(xem chi tiết:)*</summary>**`DB::table('TableName')`->`lists('FieldName')`;**<br> **`FieldName`** là tên trường muốn lấy dữ liệu của bảng TableName</details>|
|**`Lấy N bản ghi`**|<details><summary>*(xem chi tiết:)*</summary>`ModelName::chunk`(**`N`**, function ($Collection) {<br>foreach ($Collection as $set) {<br>//code <br>} <br>});</details>|<details><summary>*(xem chi tiết:)*</summary>$var_name = `DB::table('TableName')->chunk`(**`N`**, function($Collection) {<br>foreach ($Collection as $set) {<br>//code<br>}<br>});</details>|
|**`Insert 1 record`**|<details><summary>*(xem chi tiết:)*</summary>$varName = new **ModelName**;<br>$varName->**FieldName_1** = 'value1';<br>...<br>$varName->**FieldName_N** = 'valueN';<br>$varName->**save()**;</details>|<details><summary>*(xem chi tiết:)*</summary>**DB::table('TableName')->insert**([<br>**'FieldName_1'** => 'value1',<br>..<br>**'FieldName_N'** => 'valueN',<br>]);</details>|
|**`Update 1 record`**<br> theo ID|<details><summary>*(xem chi tiết:)*</summary>$varName = **ModelName::find(id_value)**;<br>$varName->**FieldName_1** ='value1';<br>..<br>$varName->**FieldName_N** ='valueN';<br>$varName->**save()**;</details>|<details><summary>*(xem chi tiết:)*</summary>**DB::table('TableName')->where('id', id_value)<br>->update**([<br>**'FieldName_1'** => 'value1',<br>..<br>**'FieldName_N'** => 'valueN',<br>]);</details>|
|**`Xóa 1 record`**<br> theo ID|<details><summary>*(xem chi tiết:)*</summary>$varName = **ModelName::find(id_value)**;<br>$varName->**delete()**;</details>|<details><summary>*(xem chi tiết:)*</summary>**DB::table('TableName')<br>->where('id','= ,id_value)<br>->delete()**;</details>|
|**`Đếm số Record`**<br> với điều kiện FieldName = value|<details><summary>*(Xem chi tiết:)*</summary>**ModelName::**where('FieldName', '%value%')**<br>->count()**;</details>|<details><summary>*(Xem chi tiết:)*</summary>**DB::table**('TableName')**<br>->count()**;</details>|
|**`Tìm giá trị lớn nhất của FieldName_2`**<br> với điều kiện FieldName = value|<details><summary>*(Xem chi tiết:)*</summary>**ModelName::**where('FieldName', '%value%')**<br>->max('FieldName_2')**;</details>|<details><summary>*(Xem chi tiết:)*</summary>**DB::table**('TableName')**<br>->max('FieldName_2')**;</details>|
|---|---|---|