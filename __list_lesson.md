# Lesson List

|Branch|Content|
|---|---|
|Master|[Install and Directory Structure](https://gitlab.com/my-laravel-7/learn/-/blob/master/huong_dan.md)|
|Lesson1|[env file](https://gitlab.com/my-laravel-7/learn/-/blob/lesson1/huong_dan.md)|
|Lesson2|[Config](https://gitlab.com/my-laravel-7/learn/-/blob/lesson2/huong_dan.md)|
|Lesson3|[Maintain Down/Up](https://gitlab.com/my-laravel-7/learn/-/blob/lesson3/huong_dan.md)|
|Lesson4|[Router](https://gitlab.com/my-laravel-7/learn/-/blob/lesson4/huong_dan.md)|
|Lesson5|[Middleware](https://gitlab.com/my-laravel-7/learn/-/blob/lesson5/huong_dan.md)|
|||
|||
|||
|||
|||
